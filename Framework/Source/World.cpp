//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "World.h"
#include "Renderer.h"
#include "ParsingHelper.h"

#include "StaticCamera.h"
#include "FirstPersonCamera.h"
#include "CubeModel.h"
#include "CubeWithTexModel.h"
#include "HillModel.h"
#include "SphereModel.h"
#include "TrapezoidModel.h"
#include "HouseModel.h"
#include "PalmTree.h"
#include "PalmLeaf.h"
#include "CowBeam.h"
#include "SheepModel.h"
#include "WaterModel.h"
#include "SkyBoxModel.h"
#include "ChairModel.h"
#include "TableModel.h"
#include "ShelveModel.h"
#include "FanModel.h"
#include "UfoModel.h"
#include "Animation.h"
#include "Billboard.h"
#include <GLFW/glfw3.h>
#include "EventManager.h"
#include "TextureLoader.h"
#include "AlternativeLeaves.h"
#include "PalmTree.h"
#include "PalmLeaf.h"
#include "CoconutModel.h"
#include "OtherTree.h"

#include "ParticleDescriptor.h"
#include "ParticleEmitter.h"
#include "ParticleSystem.h"
#include <glm/gtc/random.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>


using namespace std;
using namespace glm;

World* World::instance;


World::World(): mSkybox(nullptr)
{
    instance = this;
    
    // Setup Camera
    mCamera.push_back(new FirstPersonCamera(vec3(3.0f, 1.0f, 5.0f)));
    mCamera.push_back(new StaticCamera(vec3(3.0f, 30.0f, 5.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f)));
    mCamera.push_back(new StaticCamera(vec3(0.5f,  0.5f, 5.0f), vec3(0.0f, 0.5f, 0.0f), vec3(0.0f, 1.0f, 0.0f)));
    mCurrentCamera = 0;
    
    
    // TODO: You can play with different textures by changing the billboardTest.bmp to another texture
#if defined(PLATFORM_OSX)
    //    int billboardTextureID = TextureLoader::LoadTexture("Textures/BillboardTest.bmp");
    int billboardTextureID = TextureLoader::LoadTexture("Textures/Particle.png");
#else
    //    int billboardTextureID = TextureLoader::LoadTexture("../Assets/Textures/BillboardTest.bmp");
    int billboardTextureID = TextureLoader::LoadTexture("../Assets/Textures/Particle.png");
#endif
    assert(billboardTextureID != 0);
    
    mpBillboardList = new BillboardList(2048, billboardTextureID);
}

World::~World()
{
    // Models
    for (vector<Model*>::iterator it = mModel.begin(); it < mModel.end(); ++it)
        delete *it;
    mModel.clear();
    
    
    for (vector<Animation*>::iterator it = mAnimation.begin(); it < mAnimation.end(); ++it)
    {
        delete *it;
    }
    
    mAnimation.clear();
    
    for (vector<AnimationKey*>::iterator it = mAnimationKey.begin(); it < mAnimationKey.end(); ++it)
    {
        delete *it;
    }
    
    mAnimationKey.clear();
    
    // Camera
    for (vector<Camera*>::iterator it = mCamera.begin(); it < mCamera.end(); ++it)
    {
        delete *it;
    }
    mCamera.clear();
    
    delete mpBillboardList;
}

World* World::GetInstance()
{
    return instance;
}

void World::Update(float dt)
{
    // User Inputs
    // 0 1 2 to change the Camera
    if (glfwGetKey(EventManager::GetWindow(), GLFW_KEY_1 ) == GLFW_PRESS)
    {
        mCurrentCamera = 0;
    }
    else if (glfwGetKey(EventManager::GetWindow(), GLFW_KEY_2 ) == GLFW_PRESS)
    {
        if (mCamera.size() > 1)
        {
            mCurrentCamera = 1;
        }
    }
    else if (glfwGetKey(EventManager::GetWindow(), GLFW_KEY_3 ) == GLFW_PRESS)
    {
        if (mCamera.size() > 2)
        {
            mCurrentCamera = 2;
        }
    }
    
    // Spacebar to change the shader
    if (glfwGetKey(EventManager::GetWindow(), GLFW_KEY_0 ) == GLFW_PRESS)
    {
        Renderer::SetShader(SHADER_SOLID_COLOR);
    }
    else if (glfwGetKey(EventManager::GetWindow(), GLFW_KEY_9 ) == GLFW_PRESS)
    {
        Renderer::SetShader(SHADER_BLUE);
    }
    
    // Update animation and keys
    for (vector<Animation*>::iterator it = mAnimation.begin(); it < mAnimation.end(); ++it)
    {
        (*it)->Update(dt);
    }
    
    for (vector<AnimationKey*>::iterator it = mAnimationKey.begin(); it < mAnimationKey.end(); ++it)
    {
        (*it)->Update(dt);
    }
    
    
    // Update current Camera
    mCamera[mCurrentCamera]->Update(dt);
    
    // Update models
    for (vector<Model*>::iterator it = mModel.begin(); it < mModel.end(); ++it)
    {
        (*it)->Update(dt);
    }
    
    // Update billboards
    
    for (vector<ParticleSystem*>::iterator it = mParticleSystemList.begin(); it != mParticleSystemList.end(); ++it)
    {
        (*it)->Update(dt);
    }
    
    mpBillboardList->Update(dt);
    
    if (mSkybox)
    {
        mSkybox->Update(dt);
    }
    
}

void World::Draw()
{
    Renderer::BeginFrame();
    
    // Set shader to use
    glUseProgram(Renderer::GetShaderProgramID());
    unsigned int prevShader = Renderer::GetCurrentShader();
    
    // Send the view projection constants to the shader
    mat4 VP = mCamera[mCurrentCamera]->GetViewProjectionMatrix();
    
    // This looks for the MVP Uniform variable in the Vertex Program
    GLuint VPMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "ViewProjectionTransform");
    glUniformMatrix4fv(VPMatrixLocation, 1, GL_FALSE, &VP[0][0]);
    
    // Light Source Location
	GLuint LightSourceLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "ViewTransform");
	mat4 V = mCamera[mCurrentCamera]->GetViewMatrix();
	glUniformMatrix4fv(LightSourceLocation, 1, GL_FALSE, &V[0][0]);
    
    if (mSkybox)
    {
        glDisable(GL_DEPTH_TEST);
        mSkybox->Draw();
        glEnable(GL_DEPTH_TEST);
    }
    
    // Draw models
    for (vector<Model*>::iterator it = mModel.begin(); it < mModel.end(); ++it)
    {
        (*it)->Draw();
    }
    
    Renderer::CheckForErrors();
    
    // Draw Billboards
    mpBillboardList->Draw();
    
    
    // Restore previous shader
    Renderer::SetShader((ShaderType) prevShader);
    
    Renderer::EndFrame();
}

void World::LoadScene(const char * scene_path)
{
    // Using case-insensitive strings and streams for easier parsing
    ci_ifstream input;
    input.open(scene_path, ios::in);
    
    // Invalid file
    if(input.fail() )
    {
        fprintf(stderr, "Error loading file: %s\n", scene_path);
        getchar();
        exit(-1);
    }
    
    ci_string item;
    while( std::getline( input, item, '[' ) )
    {
        ci_istringstream iss( item );
        
        ci_string result;
        if( std::getline( iss, result, ']') )
        {
            if (result == "cube")
            {
                // Box attributes
                CubeModel* cube = new CubeModel();
                cube->Load(iss);
                mModel.push_back(cube);
            }
            else if (result == "cubewithtexmodel") // Emir Bozer: adding a new object type
                //It refers to the cubetextmodel.cpp and .h
            {
                // Box attributes
                CubeWithTexModel* cube = new CubeWithTexModel();
                cube->Load(iss);
                mModel.push_back(cube);
            }
            else if (result == "skybox")
            {
                SkyBoxModel* skybox = new SkyBoxModel();
                skybox->Load(iss);
                //mModel.push_back(skybox);
                mSkybox = skybox;
            }
            else if (result == "hill")
            {
                HillModel* hill = new HillModel();
                hill->Load(iss);
                mModel.push_back(hill);
            }
            else if (result == "house")
            {
                HouseModel* house = new HouseModel();
                house->Load(iss);
                mModel.push_back(house);
            }
            else if (result == "trapezoid")
            {
                TrapezoidModel* trapezoid = new TrapezoidModel();
                trapezoid->Load(iss);
                mModel.push_back(trapezoid);
            }
            else if (result == "sheep"){
                SheepModel* sheep = new SheepModel();
                sheep->Load(iss);
                mModel.push_back(sheep);
            }
            else if ( result == "cowbeam"){
                //vec3 pos, vec3 normal, float size, float maxHeight, float original
                 //World::MakeABeam( vec3(-10.0f,-1.0f,0.0f) , vec3(0.0f,1.0f,0.0f) , 3.0f, 10.0f,1.0f);
				//vec3 pos = vec3(-10.0f, -1.0f, 0.0f);
				//vec3 normal = vec3(0.0f, 1.0f, 0.0f);
				float size = 2.0f;
				float maxHeight = 10.0f;
				//float original = 1.0f;
				 if (maxHeight>0){
					 //normal = normalize(normal);
					 CowBeam* myBeam = new CowBeam(size);
					 //myBeam->SetPosition(pos);
					 myBeam->Load(iss);
					 mModel.push_back(myBeam);
					 //MakeABeam(pos + normal*myBeam->GetHeight(), normal, size * myBeam->GetTrunkScale(), maxHeight - myBeam->GetHeight(), original);
				 }
            }
            else if (result == "tree"){
         
				//good palm tests
				World::MakeAPalmTree(vec3(-18.0f, -1.5f, -10.0f), vec3(0.0f, 1.0f, 0.0f), 1.0f, 10.0f, 1.0f, 6, 10.0f);
				World::MakeAPalmTree(vec3(-10.0f, -1.5f, -15.0f), vec3(0.0f, 1.0f, 0.0f), 0.5f, 5.0f, 1.25f, 5, 5.0f);
				World::MakeAPalmTree(vec3(-12.0f, -1.5f, -18.0f), vec3(0.0f, 1.0f, 0.0f), 1.5f, 15.0f, 1.5f, 7, 15.0f);

				//pine tests
				World::MakeAOtherTree(vec3(-20.0f, -1.5f, -12.0f), vec3(0.0f, 1.0f, 0.0f), 1.0f, 10.0f, 2.0f, 7, 10.0f, angleAxis(0.0f, vec3(0, 1, 0)));
				World::MakeAOtherTree(vec3(-14.0f, -1.5f, -20.0f), vec3(0.0f, 1.0f, 0.0f), 0.9f, 10.0f, 2.0f, 7, 10.0f, angleAxis(0.0f, vec3(0, 1, 0)));
				World::MakeAOtherTree(vec3(-16.0f, -1.5f, -15.0f), vec3(0.0f, 1.0f, 0.0f), 0.5f, 8.0f, 2.0f, 7, 10.0f, angleAxis(0.0f, vec3(0, 1, 0)));

            }
            else if( result == "sphere" )
            {
                SphereModel* sphere = new SphereModel();
                sphere->Load(iss);
                mModel.push_back(sphere);
            }
            else if ( result == "animationkey" )
            {
                AnimationKey* key = new AnimationKey();
                key->Load(iss);
                mAnimationKey.push_back(key);
            }
            else if (result == "animation")
            {
                Animation* anim = new Animation();
                anim->Load(iss);
                mAnimation.push_back(anim);
            }
            else if (result == "chair")
            {
                ChairModel* chair = new ChairModel();
                chair->Load(iss);
                mModel.push_back(chair);
            }
            else if (result == "table")
            {
                TableModel* table = new TableModel();
                table->Load(iss);
                mModel.push_back(table);
            }
            else if (result == "shelve")
            {
                ShelveModel* shelve = new ShelveModel();
                shelve->Load(iss);
                mModel.push_back(shelve);
            }
            else if (result == "fan")
            {
                FanModel* fan = new FanModel();
                fan->Load(iss);
                mModel.push_back(fan);
            }
            else if (result == "ufo")
            {
                UfoModel* ufo = new UfoModel();
                ufo->Load(iss);
                mModel.push_back(ufo);
            }
			else if (result == "water")
			{
				WaterModel* water = new WaterModel();
				water->Load(iss);
				mModel.push_back(water);
			}
            else if ( result.empty() == false && result[0] == '#')
            {
                // this is a comment line
            }
            else
            {
                fprintf(stderr, "Error loading scene file... !");
                getchar();
                exit(-1);
            }
        }
    }
    input.close();
    
    // Set Animation vertex buffers
    for (vector<Animation*>::iterator it = mAnimation.begin(); it < mAnimation.end(); ++it)
    {
        // Draw model
        (*it)->CreateVertexBuffer();
    }
}

Animation* World::FindAnimation(ci_string animName)
{
    for(std::vector<Animation*>::iterator it = mAnimation.begin(); it < mAnimation.end(); ++it)
    {
        if((*it)->GetName() == animName)
        {
            return *it;
        }
    }
    return nullptr;
}

AnimationKey* World::FindAnimationKey(ci_string keyName)
{
    for(std::vector<AnimationKey*>::iterator it = mAnimationKey.begin(); it < mAnimationKey.end(); ++it)
    {
        if((*it)->GetName() == keyName)
        {
            return *it;
        }
    }
    return nullptr;
}

const Camera* World::GetCurrentCamera() const
{
    return mCamera[mCurrentCamera];
}

void World::AddBillboard(Billboard* b)
{
    mpBillboardList->AddBillboard(b);
}

void World::RemoveBillboard(Billboard* b)
{
    mpBillboardList->RemoveBillboard(b);
}

void World::AddParticleSystem(ParticleSystem* particleSystem)
{
    mParticleSystemList.push_back(particleSystem);
}

void World::RemoveParticleSystem(ParticleSystem* particleSystem)
{
    vector<ParticleSystem*>::iterator it = std::find(mParticleSystemList.begin(), mParticleSystemList.end(), particleSystem);
    mParticleSystemList.erase(it);
}

void World::MakeAPalmTree(vec3 pos, vec3 normal, float size, float maxHeight, float leafThick, float leafNum, float totalHeight)
{
	//Made by Mario Felipe Munoz
	if (maxHeight>0){

		normal = normalize(normal);
		PalmTree* myTree = new PalmTree(size);
		myTree->SetPosition(pos);
		mModel.push_back(myTree);
		MakeAPalmTree(pos + normal*myTree->GetHeight(), normal, size * myTree->GetTrunkScale(), maxHeight - myTree->GetHeight(), leafThick, leafNum, totalHeight);

	}
	else{
		float angle = 360.0f / leafNum;
		float currAngle = 0;
		normal = normalize(normal);
		for (int p = 0; p<leafNum; p++){
			PalmLeaf* myLeaf = new PalmLeaf(leafThick);
			myLeaf->SetScaling(vec3(size*totalHeight, size*totalHeight, totalHeight*size));
			myLeaf->SetRotation(vec3(0.0f, 1.0f, 0.0f), currAngle + linearRand(-0.25*angle, 0.25*angle));
			myLeaf->SetPosition(pos);
			currAngle = currAngle + angle;
			if (currAngle >= 360){
				currAngle = currAngle - 360;
			}
			mModel.push_back(myLeaf);
		}
		float coconuts = linearRand(1.0f, 4.0f);
		for (int i = 0; i<coconuts; i++){
			CoconutModel* myCoconut = new CoconutModel();
			myCoconut->SetScaling(vec3(size*totalHeight*0.4f, size*totalHeight*0.5, totalHeight*size*0.4f));
			myCoconut->SetPosition(vec3(circularRand(size*totalHeight*0.5).x + pos.x, pos.y, circularRand(size*totalHeight*0.5).y + pos.z));
			mModel.push_back(myCoconut);
		}

	}
}


void World::MakeAOtherTree(vec3 pos, vec3 normal, float size, float maxHeight, float leafThick, float leafNum, float totalHeight, quat turn)
{
	//Made by Mario Felipe Munoz
	if (maxHeight>0){
		normal = normalize(normal);
		OtherTree* myTree = new OtherTree(size);
		myTree->SetRotation(axis(turn), angle(turn));
		myTree->SetPosition(pos);
		mModel.push_back(myTree);
		MakeAOtherTree(pos + turn*normal*myTree->GetHeight(), normal, size * myTree->GetTrunkScale(), maxHeight - myTree->GetHeight(), leafThick, leafNum, totalHeight, turn);
		if (totalHeight - maxHeight >= totalHeight * 1 / 3 && linearRand(0.0, 1.0) >= 0.80){
			OtherTree* myBranch = new OtherTree(size*0.7);
			turn = angleAxis(linearRand(0.0f, 359.9f), turn*normal) * turn * angleAxis(linearRand(20.0f, 35.0f), axis(turn));
			myBranch->SetRotation(axis(turn), angle(turn));
			myBranch->SetPosition(pos);
			mModel.push_back(myBranch);
			MakeAOtherTree(pos + turn*normal*myBranch->GetHeight(), normal, size * myBranch->GetTrunkScale()*0.75, maxHeight - myBranch->GetHeight() * 3, leafThick, leafNum, totalHeight, turn);
		}
	}

	else{

		AlternativeLeaves* myDisco = new AlternativeLeaves();
		myDisco->SetScaling(vec3(0.6f));
		myDisco->SetPosition(pos);
		mModel.push_back(myDisco);

	}

}

void World::MakeABeam(vec3 pos, vec3 normal, float size, float maxHeight, float original)
{
    
    if(maxHeight>0){
        normal = normalize(normal);
        CowBeam* myBeam = new CowBeam(size);
        myBeam->SetPosition(pos);
        mModel.push_back(myBeam);
        MakeABeam(pos+normal*myBeam->GetHeight(), normal, size * myBeam->GetTrunkScale(), maxHeight - myBeam->GetHeight(),original);
    }
    
}
