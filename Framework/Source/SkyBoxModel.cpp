//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "SkyBoxModel.h"
#include "Renderer.h"

#include "FirstPersonCamera.h"
#include "Camera.h"
#include "World.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>
// Include FreeImage for texture loading
#include <FreeImageIO.h>

using namespace glm;

SkyBoxModel::SkyBoxModel(vec3 size) : Model()
{
	// Create Vertex Buffer for all the verices of the Cube
	vec3 halfSize = size * 0.5f;
    Renderer::CheckForErrors();

    Vertex vertexBuffer[] = {  // position,                                 normal,                  color
								{ vec3(-halfSize.x,-halfSize.y,-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) }, //left
								{ vec3(-halfSize.x,-halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
        
								{ vec3(-halfSize.x,-halfSize.y,-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y,-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x, halfSize.y,-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) }, // far
								{ vec3(-halfSize.x,-halfSize.y,-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y,-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) },
        
								{ vec3( halfSize.x, halfSize.y,-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
								{ vec3( halfSize.x,-halfSize.y,-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
								{ vec3(-halfSize.x,-halfSize.y,-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x,-halfSize.y, halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) }, // bottom
								{ vec3(-halfSize.x,-halfSize.y,-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3( halfSize.x,-halfSize.y,-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x,-halfSize.y, halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
								{ vec3(-halfSize.x,-halfSize.y, halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) },
								{ vec3(-halfSize.x,-halfSize.y,-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
        
								{ vec3(-halfSize.x, halfSize.y, halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) }, // near
								{ vec3(-halfSize.x,-halfSize.y, halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3( halfSize.x,-halfSize.y, halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x, halfSize.y, halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y, halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) },
								{ vec3( halfSize.x,-halfSize.y, halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x, halfSize.y, halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) }, // right
								{ vec3( halfSize.x,-halfSize.y,-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3( halfSize.x, halfSize.y,-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x,-halfSize.y,-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3( halfSize.x, halfSize.y, halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
								{ vec3( halfSize.x,-halfSize.y, halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) },
        
								{ vec3( halfSize.x, halfSize.y, halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) }, // top
								{ vec3( halfSize.x, halfSize.y,-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 0.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y,-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
        
								{ vec3( halfSize.x, halfSize.y, halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(1.0f, 1.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y,-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 0.0f, 0.0f) },
								{ vec3(-halfSize.x, halfSize.y, halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec3(0.0f, 1.0f, 0.0f) }
    };

    
#if defined(PLATFORM_OSX)
    
    texturesFilesPaths.push_back("Textures/ely_hills/hills_left.tga");
    texturesFilesPaths.push_back("Textures/ely_hills/hills_right.tga");
    texturesFilesPaths.push_back("Textures/ely_hills/hills_down.tga");
    texturesFilesPaths.push_back("Textures/ely_hills/hills_up.tga");
    texturesFilesPaths.push_back("Textures/ely_hills/hills_front.tga");
    texturesFilesPaths.push_back("Textures/ely_hills/hills_back.tga");
     /*
    texturesFilesPaths.push_back("Textures/mp_organic/organic_lf.tga");
    texturesFilesPaths.push_back("Textures/mp_organic/organic_rt.tga");
    texturesFilesPaths.push_back("Textures/mp_organic/organic_dn.tga");
    texturesFilesPaths.push_back("Textures/mp_organic/organic_up.tga");
    texturesFilesPaths.push_back("Textures/mp_organic/organic_ft.tga");
    texturesFilesPaths.push_back("Textures/mp_organic/organic_bk.tga");*/
    
    mTextureID = SkyBoxModel::loadCubeMapTexture(texturesFilesPaths);
    
#else
    texturesFilesPaths.push_back("../Assets/Textures/ely_hills/hills_left.tga");
    texturesFilesPaths.push_back("../Assets/Textures/ely_hills/hills_right.tga");
    texturesFilesPaths.push_back("../Assets/Textures/ely_hills/hills_down.tga");
    texturesFilesPaths.push_back("../Assets/Textures/ely_hills/hills_up.tga");
    texturesFilesPaths.push_back("../Assets/Textures/ely_hills/hills_front.tga");
    texturesFilesPaths.push_back("../Assets/Textures/ely_hills/hills_back.tga");
    
    mTextureID = SkyBoxModel::loadCubeMapTexture(texturesFilesPaths);
    
#endif
    assert(mTextureID != 0);

	// Create a vertex array
	glGenVertexArrays(1, &mVertexArrayID);

	// Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
    Renderer::CheckForErrors();

    glGenBuffers(1, &mVertexBufferID);
    Renderer::CheckForErrors();

    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    Renderer::CheckForErrors();

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
    
    Renderer::CheckForErrors();

}



int SkyBoxModel::loadCubeMapTexture(std::vector<const char *> imagepaths)
{
    // Get an available texture index from OpenGL
    GLuint texture = 0;
    glGenTextures(1, &texture);
    assert(texture != 0);
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    for (GLuint i = 0; i < imagepaths.size(); i++) {
        
        // Load image using the Free Image library
        FREE_IMAGE_FORMAT format = FreeImage_GetFileType(imagepaths[i], 0);
        FIBITMAP* image = FreeImage_Load(format, imagepaths[i]);
        FIBITMAP* image32bits = FreeImage_ConvertTo32Bits(image);
        Renderer::CheckForErrors();

        // Set OpenGL filtering properties (bi-linear interpolation)
        Renderer::CheckForErrors();

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        Renderer::CheckForErrors();

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        Renderer::CheckForErrors();
    
        // Retrieve width and hight
        int width = FreeImage_GetWidth(image32bits);
        int height = FreeImage_GetHeight(image32bits);
    
        // This will upload the texture to the GPU memory
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8, width, height,
                 0, GL_BGRA, GL_UNSIGNED_BYTE, (void*)FreeImage_GetBits(image32bits));
    
        Renderer::CheckForErrors();

        
        // Free images
        FreeImage_Unload(image);
        FreeImage_Unload(image32bits);
    }
    
    return texture;
}



SkyBoxModel::~SkyBoxModel()
{
	// Free the GPU from the Vertex Buffer
	glDeleteBuffers(1, &mVertexBufferID);
	glDeleteVertexArrays(1, &mVertexArrayID);
}

void SkyBoxModel::Update(float dt)
{
    mPosition = World::GetInstance()->GetCurrentCamera()->GetPosition();
    Model::Update(dt);
}

void SkyBoxModel::Draw()
{
    Renderer::CheckForErrors();
    
    ShaderType previousShader = (ShaderType)Renderer::GetCurrentShader();
    Renderer::SetShader(SHADER_SKYBOX);
    glUseProgram(Renderer::GetShaderProgramID());
    
    Renderer::CheckForErrors();
    
    GLuint textureLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "myTextureSampler");
    glActiveTexture(GL_TEXTURE0);
    
    Renderer::CheckForErrors();
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, mTextureID);
    glUniform1i(textureLocation, 0);				// Set our Texture sampler to user Texture Unit 0
    
    Renderer::CheckForErrors();
    
    // This looks for the MVP Uniform variable in the Vertex Program
    GLuint VPMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "ViewProjectionTransform");
    
    // Send the view projection constants to the shader
    const Camera* currentCamera = World::GetInstance()->GetCurrentCamera();
    mat4 VP = currentCamera->GetViewProjectionMatrix();
    glUniformMatrix4fv(VPMatrixLocation, 1, GL_FALSE, &VP[0][0]);
    
	// Draw the Vertex Buffer
	// Note this draws a unit Cube
	// The Model View Projection transforms are computed in the Vertex Shader
	glBindVertexArray(mVertexArrayID);

	GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform"); 
	glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
	
	// 1st attribute buffer : vertex Positions
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
							3,				// size
							GL_FLOAT,		// type
							GL_FALSE,		// normalized?
							sizeof(Vertex), // stride
							(void*)0        // array buffer offset
						);

	// 2nd attribute buffer : vertex normal
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	1,
							3,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
						);


	// 3rd attribute buffer : vertex color
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	2,
							3,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
						);
    
    // 4th attribute buffer : 3D texture coordinates
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	3,				// attribute. No particular reason for 0, but must match the layout in the shader.
                          3,				// size
                          GL_FLOAT,		// type
                          GL_FALSE,		// normalized?
                          sizeof(Vertex), // stride
                          (void*) (2* sizeof(vec3) + sizeof(vec4))       // array buffer offset
                          );

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, 36); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)

	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
    
    Renderer::CheckForErrors();
    
    Renderer::SetShader(previousShader);
}

bool SkyBoxModel::ParseLine(const std::vector<ci_string> &token)
{
	if (token.empty())
	{
		return true;
	}
	else
	{
		return Model::ParseLine(token);
	}
}
