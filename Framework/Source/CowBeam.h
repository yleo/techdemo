//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#pragma once

#include "Model.h"

class CowBeam : public Model
{
public:
	CowBeam(float size);
	virtual ~CowBeam();

	virtual void Update(float dt);
	virtual void Draw();
	float GetTrunkScale() const		{ return trunkScale; }
	float GetHeight() const		{ return height; }
	glm::vec3 GetRefPoint() const {return refPoint; }

protected:
	virtual bool ParseLine(const std::vector<ci_string> &token);

private:
	// The vertex format could be different for different types of models
	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec4 color;
	};

	float height;
	float trunkScale;
	int numOfVertices;
	glm::vec3 refPoint;
	unsigned int mVertexArrayID;
	unsigned int mVertexBufferID;
};
