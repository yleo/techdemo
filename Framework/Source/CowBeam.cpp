//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "CowBeam.h"
#include "Renderer.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>
#include <glm/gtc/random.hpp>
#include <glm/gtx/rotate_vector.hpp>
using namespace glm;

CowBeam::CowBeam(float size) : Model()
{
	height = 15.0;
	trunkScale = 0.4;
	// Create Vertex Buffer for all the verices of the Cube
	Vertex vertexBuffer[3600*4];
	vec2 reference = glm::circularRand(size);
	refPoint = vec3(reference.x,0,reference.y);

	for(int i = 0; i < 3600*4; i=i+4){
		vec3 primo = refPoint;
		vec3 secundo = glm::rotateY(primo, 1.0f);
		vec3 tertio = vec3(primo.x * trunkScale, height, primo.z * trunkScale);
		vec3 quarto = glm::rotateY(tertio, 1.0f);
		refPoint = glm::rotateY(refPoint,0.1f);
		vertexBuffer[i].color = vec4(1.0, 0.8, 0.0, 0.1);
		vertexBuffer[i].position = primo;
		vertexBuffer[i].normal = vec3(0.0f, 1.0f, 0.0f);
		vertexBuffer[i+1].color = vec4(1.0, 0.8, 0.0, 0.1);
		vertexBuffer[i+1].position = tertio;
		vertexBuffer[i+1].normal = vec3(0.0f, 1.0f, 0.0f);
		vertexBuffer[i+2].color = vec4(1.0, 0.8, 0.0, 0.1);
		vertexBuffer[i+2].position = secundo;
		vertexBuffer[i+2].normal = vec3(0.0f, 1.0f, 0.0f);
		vertexBuffer[i+3].color = vec4(1.0, 0.8, 0.0, 0.1);
		vertexBuffer[i+3].position = quarto;
		vertexBuffer[i+3].normal = vec3(0.0f, 1.0f, 0.0f);
	}

	numOfVertices = sizeof(vertexBuffer) / sizeof(Vertex);
	// Create a vertex array
	glGenVertexArrays(1, &mVertexArrayID);

	// Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
	glGenBuffers(1, &mVertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
}

CowBeam::~CowBeam()
{
	// Free the GPU from the Vertex Buffer
	glDeleteBuffers(1, &mVertexBufferID);
	glDeleteVertexArrays(1, &mVertexArrayID);
}

void CowBeam::Update(float dt)
{
	// If you are curious, un-comment this line to have spinning cubes!
	// That will only work if your world transform is correct...
	// mRotationAngleInDegrees += 90 * dt; // spins by 90 degrees per second

	Model::Update(dt);
}

void CowBeam::Draw()
{
	// Draw the Vertex Buffer
	// Note this draws a unit Cube
	// The Model View Projection transforms are computed in the Vertex Shader
	glBindVertexArray(mVertexArrayID);

	GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform"); 
	glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
	
	// 1st attribute buffer : vertex Positions
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
							3,				// size
							GL_FLOAT,		// type
							GL_FALSE,		// normalized?
							sizeof(Vertex), // stride
							(void*)0        // array buffer offset
						);

	// 2nd attribute buffer : vertex normal
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	1,
							3,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
						);


	// 3rd attribute buffer : vertex color
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	2,
							4,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
						);

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLE_STRIP, 5, numOfVertices); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)

	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}

bool CowBeam::ParseLine(const std::vector<ci_string> &token)
{
	if (token.empty())
	{
		return true;
	}
	else
	{
		return Model::ParseLine(token);
	}
}
