//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#pragma once


#include "ParsingHelper.h"
#include "Billboard.h"
#include <vector>

class Camera;
class Model;
class Animation;
class AnimationKey;
class ParticleSystem;
class SkyBoxModel;

class World
{
public:
	World();
	~World();
	
    static World* GetInstance();

	void Update(float dt);
	void Draw();

	void LoadScene(const char * scene_path);
    Animation* FindAnimation(ci_string animName);
	AnimationKey* FindAnimationKey(ci_string keyName);

    const Camera* GetCurrentCamera() const;
    void AddBillboard(Billboard* b);
    void RemoveBillboard(Billboard* b);
    void AddParticleSystem(ParticleSystem* particleSystem);
    void RemoveParticleSystem(ParticleSystem* particleSystem);
    
    void MakeABeam(glm::vec3 pos, glm::vec3 normal, float size, float maxHeight, float original);
	void MakeAPalmTree(glm::vec3 pos, glm::vec3 normal, float size, float maxHeight, float leafThick, float leafNum, float totalHeight);
	void MakeAOtherTree(glm::vec3 pos, glm::vec3 normal, float size, float maxHeight, float leafThick, float leafNum, float totalHeight, glm::quat turn);

    
private:
    static World* instance;
    
	std::vector<Model*> mModel;
	std::vector<Model*> mTexPolygons;
	std::vector<Animation*> mAnimation;
    std::vector<AnimationKey*> mAnimationKey;
	std::vector<Camera*> mCamera;
    std::vector<ParticleSystem*> mParticleSystemList;
	unsigned int mCurrentCamera;
    SkyBoxModel* mSkybox;

    BillboardList* mpBillboardList;
};
