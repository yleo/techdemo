//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "ChairModel.h"
#include "Renderer.h"
#include "World.h"
#include "Camera.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>

using namespace glm;

ChairModel::ChairModel(vec3 size) : Model()
{
    // Create Vertex Buffer for all the vertices of the Chair
    vec3 halfSize = size * 0.5f;
    
    Vertex vertexBuffer[] = {  // position,                normal,                  color
								// Back
								{ vec3(0.45*-halfSize.x,-0.75*halfSize.y, 0.1*-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)}, //left - red
								{ vec3(0.45*-halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(0.45*-halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
        
								{ vec3(0.45*-halfSize.x,-0.75*halfSize.y,0.1*-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3(0.45*-halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3(0.45*-halfSize.x, 0.75*halfSize.y,0.1*-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y,0.1*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // far - blue
								{ vec3( 0.45*-halfSize.x,-0.75*halfSize.y,0.1*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.45*-halfSize.x, 0.75*halfSize.y,0.1*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
        
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y,0.1*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y,0.1*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3( 0.45*-halfSize.x,-0.75*halfSize.y,0.1*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
        
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // bottom - turquoise
								{ vec3( 0.45*-halfSize.x,-0.75*halfSize.y, 0.1*-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.45*-halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
								{ vec3( 0.45*-halfSize.x,-0.75*halfSize.y, 0.1*-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								
								{ vec3( 0.45*-halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)}, // near - green
								{ vec3( 0.45*-halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3( 0.45*-halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // right - purple
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.45*halfSize.x,-0.75*halfSize.y, 0.1*halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
        
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // top - yellow
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
								{ vec3( 0.45*-halfSize.x, 0.75*halfSize.y, 0.1*-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
        
								{ vec3( 0.45*halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.45*-halfSize.x, 0.75*halfSize.y, 0.1*-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.45*-halfSize.x, 0.75*halfSize.y, 0.1*halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
								
								// Seat
								{ vec3(0.5*-halfSize.x,-0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)}, //left - red
								{ vec3(0.5*-halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
        
								{ vec3(0.5*-halfSize.x,-0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3(0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3(0.5*-halfSize.x, 0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // far - blue
								{ vec3( 0.5*-halfSize.x,-0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.5*-halfSize.x, 0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
        
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3( 0.5*-halfSize.x,-0.1*halfSize.y-0.4,0.45*-halfSize.z + 0.17), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
        
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // bottom - turquoise
								{ vec3( 0.5*-halfSize.x,-0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.5*-halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
								{ vec3( 0.5*-halfSize.x,-0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
        
								{ vec3( 0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)}, // near - green
								{ vec3( 0.5*-halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3( 0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // right - purple
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
        
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.5*halfSize.x,-0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
        
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)}, // top - yellow
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f)},
								{ vec3( 0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
        
								{ vec3( 0.5*halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f)},
								{ vec3( 0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*-halfSize.z + 0.17), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f)},
								{ vec3( 0.5*-halfSize.x, 0.1*halfSize.y-0.4, 0.45*halfSize.z + 0.17), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f)},
        
								// Leg Front Right
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) }, //left - red
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
        
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // far - blue
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*-halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) }, // near - green
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*-halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // right - purple
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // top - yellow
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								// Leg Front Left
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) }, //left - red
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
        
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // far - blue
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*-halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) }, // near - green
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*-halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // right - purple
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // top - yellow
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(1.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z + 0.3), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								// Leg Back Right
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) }, //left - red
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
        
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // far - blue
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*-halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) }, // near - green
								{ vec3(-0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*-halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // right - purple
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x + 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // top - yellow
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x + 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								// Leg Back Left
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) }, //left - red
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
        
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // far - blue
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*-halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) }, // near - green
								{ vec3(-0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*-halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // right - purple
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(0.1*halfSize.x - 0.19, -0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) }, // top - yellow
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
        
								{ vec3(0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(1.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(1.0f, 1.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, -0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 0.0f) },
								{ vec3(-0.1*halfSize.x - 0.19, 0.5*halfSize.y - 0.65, 0.1*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f,  0.5f), vec2(0.0f, 1.0f) }
        
    };
    
    // Create a vertex array
    glGenVertexArrays(1, &mVertexArrayID);
    
    // Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
    glGenBuffers(1, &mVertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
}

ChairModel::~ChairModel()
{
    // Free the GPU from the Vertex Buffer
    glDeleteBuffers(1, &mVertexBufferID);
    glDeleteVertexArrays(1, &mVertexArrayID);
}

void ChairModel::Update(float dt)
{
    // If you are curious, un-comment this line to have spinning Chairs!
    // That will only work if your world transform is correct...
    // mRotationAngleInDegrees += 90 * dt; // spins by 90 degrees per second
    
    Model::Update(dt);
}

void ChairModel::Draw()
{
    // Draw the Vertex Buffer
    // Note this draws a unit Chair
    ShaderType oldShader = (ShaderType)Renderer::GetCurrentShader();
    Renderer::SetShader(SHADER_TEXTURED);
    glUseProgram(Renderer::GetShaderProgramID());
    
    
    // This looks for the MVP Uniform variable in the Vertex Program
    GLuint VPMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "ViewProjectionTransform");
    
    // Send the view projection constants to the shader
    const Camera* currentCamera = World::GetInstance()->GetCurrentCamera();
    mat4 VP = currentCamera->GetViewProjectionMatrix();
    glUniformMatrix4fv(VPMatrixLocation, 1, GL_FALSE, &VP[0][0]);
    
    
    GLuint textureLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "myTextureSampler");
    glActiveTexture(GL_TEXTURE0);
    Renderer::CheckForErrors();
    glBindTexture(GL_TEXTURE_2D, mTexID);
    //glEnable(GL_TEXTURE_2D);
    glUniform1i(textureLocation, 0);
    
    
    // The Model View Projection transforms are computed in the Vertex Shader
    glBindVertexArray(mVertexArrayID);
    
    GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform");
    glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
    
    // 1st attribute buffer : vertex Positions
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
                          3,				// size
                          GL_FLOAT,		// type
                          GL_FALSE,		// normalized?
                          sizeof(Vertex), // stride
                          (void*)0        // array buffer offset
                          );
    
    // 2nd attribute buffer : vertex normal
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	1,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(Vertex),
                          (void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
                          );
    
    
    // 3rd attribute buffer : vertex color
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	2,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(Vertex),
                          (void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
                          );
    // 4 attribute buffer : texture coord
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	3,
                          2,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(Vertex),
                          (void*)(2 * sizeof(vec3) + sizeof(vec4)) // Color is Offseted by 2 vec3 (see class Vertex)
                          );
    
    // Draw the triangles !
    glDrawArrays(GL_TRIANGLES, 0, 216); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)
    
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
    
    Renderer::SetShader(oldShader);
    glUseProgram(Renderer::GetShaderProgramID());
}

bool ChairModel::ParseLine(const std::vector<ci_string> &token)
{
    if (token.empty())
    {
        return true;
    }
    else
    {
        return Model::ParseLine(token);
    }
}
