//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "HouseModel.h"
#include "Renderer.h"
#include "World.h"
#include "Camera.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>

using namespace glm;

HouseModel::HouseModel(vec3 size) : Model()
{
	// Create Vertex Buffer for all the verices of the Cube
	vec3 halfSize = size * 0.5f;
    
   
	
	Vertex vertexBuffer[] = {  // position,                normal,                  color
                //[0][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+12, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+12, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
    
        { vec3(halfSize.x+12, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+12, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+14, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+14, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+12, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+14, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
  //WINDOWS
        { vec3(halfSize.x+12, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.0f) },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.25f) },
        { vec3(halfSize.x+16, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)},
        
        { vec3(halfSize.x+16, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.25f)  },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.25f)  },
   //WINDOWS ****
        { vec3(halfSize.x+14, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+14, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+16, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+16, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+14, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+16, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+16, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+18, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+18, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+18, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+20, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+20, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+20, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+20, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+22, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+22, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+20, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+22, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+22, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+22, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+24, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+24, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+22, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+24, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+24, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+26, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+26, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+26, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        //2nd row
        { vec3(halfSize.x, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
   //WINDOS
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.25f) },
        { vec3(halfSize.x+12, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.25f)},
        
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.25f)  },
        { vec3(halfSize.x+12, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f)  },
        { vec3(halfSize.x+16, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
   //***WINDOWS
        { vec3(halfSize.x+12, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+14, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x+14, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+14, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+16, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x+16, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+20, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+22, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x+22, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        //WINDOWS
         
        { vec3(halfSize.x+20, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.0f) },
        { vec3(halfSize.x+20, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+24, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)},
        
        { vec3(halfSize.x+24, halfSize.y+1, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+20, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f)  },
        { vec3(halfSize.x+24, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        //WINDOWS ****
        { vec3(halfSize.x+22, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+24, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x+24, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },

        { vec3(halfSize.x, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+2, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+2, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+12, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+12, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+12, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+14, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+14, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+14, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+16, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+16, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+16, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+18, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+18, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+18, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+20, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+20, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+20, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+22, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+22, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+22, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+24, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+24, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+24, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+26, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+26, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+26, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        //SIDE OF THE OUTSIDE FRONT LEFT WALL OF THE HOUSE
        { vec3(halfSize.x, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.25f)  },
        
        //LEFT SIDE OUTSIDE WALL OF THE HOUSE
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        //WINDOWS
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        //***WINDOWS
        
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0) },
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },

        //
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },

        { vec3(halfSize.x, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+1, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)},
        
        { vec3(halfSize.x, halfSize.y+3, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.25f)  },
        
        //BACK OUTSIDE WALL OF THE HOUSE
        
        { vec3(halfSize.x, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+12, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+12, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+12, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+14, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+14, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+14, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+14, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+14, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+16, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+16, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+14, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+16, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+18, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+18, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+18, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+20, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+20, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+20, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+22, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+22, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+22, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+22, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+22, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+24, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+24, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+22, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+24, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+26, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+26, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+26, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        //2nd row
        { vec3(halfSize.x, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+12, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+14, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+14, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+14, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+16, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+18, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+20, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+22, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+22, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+22, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+24, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+26, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+2, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+2, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+2, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+12, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+12, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+12, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+12, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+14, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+14, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+14, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+14, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+16, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+16, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+16, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+16, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+18, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+18, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+18, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+18, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+20, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+20, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+20, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+20, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+22, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+22, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+22, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+22, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+24, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+24, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+24, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+24, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+26, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+26, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+26, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+26, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        

        //RIGHT SIDE OUTSIDE WALL OF THE HOUSE
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-14), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.25f)  },
        
        //SIDE OF THE OUTSIDE FRONT RIGHT WALL OF THE HOUSE
        { vec3(halfSize.x+28, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+28, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+28, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.25f)  },
        
        //INSIDE WALL OF THE HOUSE
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)},
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 1.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
       
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)},
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 1.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
         
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+3, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)},
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.75f)  },
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
         
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)},
        
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
 //BACK
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)},
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 1.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+8, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)},
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 1.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+8, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+3, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)},
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.75f)  },
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+8, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+8, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+6, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },

 //LEFT
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+1, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+1, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
//RIGHT
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
 
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+10, halfSize.y+3, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+3, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)},
        
        { vec3(halfSize.x+10, halfSize.y+3, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-2.77), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+10, halfSize.y+3, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+3, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)},
        
        { vec3(halfSize.x+10, halfSize.y+3, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
      
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
      
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-7.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+2, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 1.0f)  },
      
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f) },
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)},
        
        { vec3(halfSize.x+10, halfSize.y+4, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.75f)  },
        { vec3(halfSize.x+10, halfSize.y+5, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.75f)  },

        

        
        //WALL BETWEEN THE DOOR
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.25f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f) },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.5f)  },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.5f)  },
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f) },
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y+2, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.25f)  },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.165f, 0.25f)  },
        
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.125f) },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)},
        
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.125f)  },
        { vec3(halfSize.x+6, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.125f)  },

        
        //DOOR
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f) },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4.5, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f) },
        { vec3(halfSize.x+4.5, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4.5, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.25f)  },
    
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f) },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4.5, halfSize.y+2, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f) },
        { vec3(halfSize.x+4.5, halfSize.y+2, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4.5, halfSize.y+2, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.25f)  },
        
        
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f) },
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f) },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z-0.25), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.5f)  },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.0f) },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f) },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)},
        
        { vec3(halfSize.x+4.5, halfSize.y+3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.0f)  },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.33f, 0.25f)  },
        { vec3(halfSize.x+4.5, halfSize.y, halfSize.z+1.75), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.25f)  },
        
        //FLOOR
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.67f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.9f, 1.0f)  },
        
//CEILLING
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-9), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 1.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+1, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+3, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+5, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f) },
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+7, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 0.5f)  },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.66f, 1.0f)  },
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 0.5f) },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f) },
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)},
        
        { vec3(halfSize.x+9, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.34f, 1.0f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-11), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 0.5f)  },
        { vec3(halfSize.x+10.5, halfSize.y+4.9, halfSize.z-13), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.5f, 1.0f)  },
        
        
        


        
    };

	// Create a vertex array
	glGenVertexArrays(1, &mVertexArrayID);

	// Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
	glGenBuffers(1, &mVertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
}

HouseModel::~HouseModel()
{
	// Free the GPU from the Vertex Buffer
	glDeleteBuffers(1, &mVertexBufferID);
	glDeleteVertexArrays(1, &mVertexArrayID);
}

void HouseModel::Update(float dt)
{

	Model::Update(dt);
}

void HouseModel::Draw()
{
    ShaderType oldShader = (ShaderType)Renderer::GetCurrentShader();
    Renderer::SetShader(SHADER_TEXTURED);
    glUseProgram(Renderer::GetShaderProgramID());
    
    
    // This looks for the MVP Uniform variable in the Vertex Program
    GLuint VPMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "ViewProjectionTransform");
    
    // Send the view projection constants to the shader
    const Camera* currentCamera = World::GetInstance()->GetCurrentCamera();
    mat4 VP = currentCamera->GetViewProjectionMatrix();
    glUniformMatrix4fv(VPMatrixLocation, 1, GL_FALSE, &VP[0][0]);
    
    
    GLuint textureLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "myTextureSampler");
    glActiveTexture(GL_TEXTURE0);
    Renderer::CheckForErrors();
    glBindTexture(GL_TEXTURE_2D, mTexID);

    //glEnable(GL_TEXTURE_2D);
    glUniform1i(textureLocation, 0);
    
   
  
    
    glBindVertexArray(mVertexArrayID);

	GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform"); 
	glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
	
    
    
    // 1st attribute buffer : vertex Positions
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
							4,				// size
							GL_FLOAT,		// type
							GL_FALSE,		// normalized?
							sizeof(Vertex), // stride
							(void*)0        // array buffer offset
						);

	// 2nd attribute buffer : vertex normal
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	1,
							4,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
						);


	// 3rd attribute buffer : vertex color
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	2,
							4,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
						);
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(  3,
                            2,
                            GL_FLOAT,
                            GL_FALSE,
                            sizeof(Vertex),
                            (void*) (2*sizeof(vec3) + sizeof(vec4))
                        );

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, 2048); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)
    
    glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
    Renderer::SetShader(oldShader);
    glUseProgram(Renderer::GetShaderProgramID());
}

bool HouseModel::ParseLine(const std::vector<ci_string> &token)
{
	if (token.empty())
	{
		return true;
	}
	else
	{
		return Model::ParseLine(token);
	}
}
