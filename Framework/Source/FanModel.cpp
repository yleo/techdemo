//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "FanModel.h"
#include "Renderer.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>

using namespace glm;

FanModel::FanModel(vec3 size) : Model()
{
    // Create Vertex Buffer for all the vertices of the Fan
    vec3 halfSize = size * 0.5f;
    
    Vertex vertexBuffer[] = {  // position,                normal,                  color
								// Rope
								{ vec3(0.05*-halfSize.x,-0.5*halfSize.y, 0.05*-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)}, //left - red
								{ vec3(0.05*-halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f) },
								{ vec3(0.05*-halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3(0.05*-halfSize.x,-0.5*halfSize.y,0.05*-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3(0.05*-halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3(0.05*-halfSize.x, 0.5*halfSize.y,0.05*-halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y,0.05*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)}, // far - blue
								{ vec3( 0.05*-halfSize.x,-0.5*halfSize.y,0.05*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*-halfSize.x, 0.5*halfSize.y,0.05*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y,0.05*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y,0.05*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f) },
								{ vec3( 0.05*-halfSize.x,-0.5*halfSize.y,0.05*-halfSize.z), vec3( 0.0f, 0.0f,-1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)}, // bottom - turquoise
								{ vec3( 0.05*-halfSize.x,-0.5*halfSize.y, 0.05*-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*-halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*-halfSize.x,-0.5*halfSize.y, 0.05*-halfSize.z), vec3( 0.0f,-1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								
								{ vec3( 0.05*-halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)}, // near - green
								{ vec3( 0.05*-halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f) },
								{ vec3( 0.05*-halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f) },
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 0.0f, 1.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f) },
        
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)}, // right - purple
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*-halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*halfSize.x,-0.5*halfSize.y, 0.05*halfSize.z), vec3( 1.0f, 0.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)}, // top - yellow
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*-halfSize.x, 0.5*halfSize.y, 0.05*-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
        
								{ vec3( 0.05*halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*-halfSize.x, 0.5*halfSize.y, 0.05*-halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								{ vec3( 0.05*-halfSize.x, 0.5*halfSize.y, 0.05*halfSize.z), vec3( 0.0f, 1.0f, 0.0f), vec4(0.10f, 0.10f, 0.10f, 1.0f)},
								
								// Center
								{ vec3(0.25*-halfSize.x,-0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)}, //left - red
								{ vec3(0.25*-halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f) },
								{ vec3(0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3(0.25*-halfSize.x,-0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3(0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3(0.25*-halfSize.x, 0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)}, // far - blue
								{ vec3( 0.25*-halfSize.x,-0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*-halfSize.x, 0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f) },
								{ vec3( 0.25*-halfSize.x,-0.25*halfSize.y-0.25,0.25*-halfSize.z ), vec3( 0.0f, 0.0f,-1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)}, // bottom - turquoise
								{ vec3( 0.25*-halfSize.x,-0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*-halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*-halfSize.x,-0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 0.0f,-1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)}, // near - green
								{ vec3( 0.25*-halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f) },
								{ vec3( 0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f) },
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 0.0f, 1.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f) },
        
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)}, // right - purple
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*halfSize.x,-0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 1.0f, 0.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)}, // top - yellow
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								{ vec3( 0.25*halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*-halfSize.z ), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
								{ vec3( 0.25*-halfSize.x, 0.25*halfSize.y-0.25, 0.25*halfSize.z ), vec3( 0.0f, 1.0f, 0.0f), vec4(0.50f, 0.50f, 0.50f, 1.0f)},
        
								// Right Connector
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, //left - red
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // far - blue
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*-halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // near - green
								{ vec3(-0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*-halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // right - purple
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x + 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // top - yellow
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x + 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z ), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								// Left Connector
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, //left - red
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // far - blue
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*-halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // near - green
								{ vec3(-0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*-halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // right - purple
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x - 0.17, -0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // top - yellow
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, -0.10*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x - 0.17, 0.10*halfSize.y - 0.25, 0.10*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								//  Near Connector
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, //left - red
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // far - blue
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*-halfSize.z - 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // near - green
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*-halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // right - purple
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // top - yellow
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z - 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z - 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								// Far Connector
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, //left - red
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(-1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // far - blue
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, -1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*-halfSize.z + 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, -1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // near - green
								{ vec3(-0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*-halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 0.0f, 1.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // right - purple
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(0.10*halfSize.x , -0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(1.0f, 0.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) }, // top - yellow
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								{ vec3(0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, -0.10*halfSize.z + 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
								{ vec3(-0.10*halfSize.x , 0.10*halfSize.y - 0.25, 0.10*halfSize.z + 0.17), vec3(0.0f, 1.0f, 0.0f), vec4(0.50f, 0.25f, 0.20f, 1.0f) },
        
								// Right Blade
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) }, //left - red
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) }, // far - blue
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) }, // bottom - turquoise
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) }, // near - green
								{ vec3(-1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*-halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) }, // right - purple
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x + 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) }, // top - yellow
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x + 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f , 0.75f, 1.0f) },
        
								// Left Blade
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, //left - red
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // far - blue
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // bottom - turquoise
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // near - green
								{ vec3(-1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*-halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // right - purple
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(1.5*halfSize.x - 0.94, -0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // top - yellow
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, -0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-1.5*halfSize.x - 0.94, 0.12*halfSize.y - 0.25, 0.35*halfSize.z), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								// Far Blade
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, //left - red
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // far - blue
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // near - green
								{ vec3(-0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*-halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // right - purple
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x , -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // top - yellow
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x , 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, //left - red
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // far - blue
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // near - green
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*-halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // right - purple
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // top - yellow
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z - 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								// Near Blade
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, //left - red
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // far - blue
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // near - green
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*-halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // right - purple
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // top - yellow
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, //left - red
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(-1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // far - blue
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, -1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // bottom - turquoise
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, -1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // near - green
								{ vec3(-0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*-halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 0.0f, 1.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // right - purple
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(0.35*halfSize.x, -0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(1.0f, 0.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }, // top - yellow
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
        
								{ vec3(0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, -1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) },
								{ vec3(-0.35*halfSize.x, 0.12*halfSize.y - 0.25, 1.5*halfSize.z + 0.94), vec3(0.0f, 1.0f, 0.0f), vec4(0.75f, 0.75f, 0.75f, 1.0f) }
        
    };
    
    // Create a vertex array
    glGenVertexArrays(1, &mVertexArrayID);
    
    // Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
    glGenBuffers(1, &mVertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
}

FanModel::~FanModel()
{
    // Free the GPU from the Vertex Buffer
    glDeleteBuffers(1, &mVertexBufferID);
    glDeleteVertexArrays(1, &mVertexArrayID);
}

void FanModel::Update(float dt)
{
    // If you are curious, un-comment this line to have spinning Fans!
    // That will only work if your world transform is correct...
    // mRotationAngleInDegrees += 90 * dt; // spins by 90 degrees per second
    
    Model::Update(dt);
}

void FanModel::Draw()
{
    // Draw the Vertex Buffer
    // Note this draws a unit Fan
    // The Model View Projection transforms are computed in the Vertex Shader
    glBindVertexArray(mVertexArrayID);
    
    GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform");
    glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
    
    // Lighting
    // Material Coefficients
    const float ka = 0.2f;
    const float kd = 0.5f;
    const float ks = 0.3f;
    const float n = 90.0f;
    
    // Light Coefficients
    const vec3 lightColor(1.0f, 1.0f, 1.0f);
    const float lightKc = 0.0f;
    const float lightKl = 0.0f;
    const float lightKq = 1.0f;
    
    const vec4 lightPosition(10.0f, -1.0f, 2.0f, 1.0f); // If w = 1.0f, we have a point light
    
    // Get a handle for Light Attributes uniform
    GLuint LightPositionID = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldLightPosition");
    GLuint LightColorID = glGetUniformLocation(Renderer::GetShaderProgramID(), "lightColor");
    GLuint LightAttenuationID = glGetUniformLocation(Renderer::GetShaderProgramID(), "lightAttenuation");
    
    // Get a handle for Material Attributes uniform
    GLuint MaterialAmbientID = glGetUniformLocation(Renderer::GetShaderProgramID(), "materialAmbient");
    GLuint MaterialDiffuseID = glGetUniformLocation(Renderer::GetShaderProgramID(), "materialDiffuse");
    GLuint MaterialSpecularID = glGetUniformLocation(Renderer::GetShaderProgramID(), "materialSpecular");
    GLuint MaterialExponentID = glGetUniformLocation(Renderer::GetShaderProgramID(), "materialExponent");
    
    // Set shader constants
    glUniform1f(MaterialAmbientID, ka);
    glUniform1f(MaterialDiffuseID, kd);
    glUniform1f(MaterialSpecularID, ks);
    glUniform1f(MaterialExponentID, n);
    
    glUniform4f(LightPositionID, lightPosition.x, lightPosition.y, lightPosition.z, lightPosition.w);
    glUniform3f(LightColorID, lightColor.r, lightColor.g, lightColor.b);
    glUniform3f(LightAttenuationID, lightKc, lightKl, lightKq);
    
    // 1st attribute buffer : vertex Positions
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
                          3,				// size
                          GL_FLOAT,		// type
                          GL_FALSE,		// normalized?
                          sizeof(Vertex), // stride
                          (void*)0        // array buffer offset
                          );
    
    // 2nd attribute buffer : vertex normal
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	1,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(Vertex),
                          (void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
                          );
    
    
    // 3rd attribute buffer : vertex color
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(	2,
                          3,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(Vertex),
                          (void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
                          );
    
    // Draw the triangles !
    glDrawArrays(GL_TRIANGLES, 0, 396); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)
    
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}

bool FanModel::ParseLine(const std::vector<ci_string> &token)
{
    if (token.empty())
    {
        return true;
    }
    else
    {
        return Model::ParseLine(token);
    }
}
