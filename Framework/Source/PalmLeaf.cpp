//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//
// Created using one of the Models made by Nicolas Bergeron as a template.
// Made by Mario Felipe Munoz

#include "PalmLeaf.h"
#include "Renderer.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>
#include <glm/gtc/random.hpp>
#include <glm/gtx/rotate_vector.hpp>
using namespace glm;

PalmLeaf::PalmLeaf(float size) : Model()
{
	
	trunkScale = 0.9f;
	// Create Vertex Buffer for all the verices of the Cube
	Vertex vertexBuffer[777];
	vec2 reference = glm::circularRand(size);
	vec3 newReference = vec3(0.0f,0.0f,0.0f);
	vec3 primo;
	vec3 secundo;
	vec3 tertio;
	float currWidth= 0.0f;
	float currDecrease=0.01f;
	float fadeDecrease=1.0;
	vec3 tempPrimo;
	bool flip = true;
	for(int i = 0; i < 777; i=i+3){

		secundo = vec3(newReference.x, newReference.y+size, newReference.z - 0.1f);
		tertio = vec3(newReference.x, newReference.y+size, newReference.z + 0.1f);

		if(flip){
			primo = newReference;
			if(currWidth < size){
				primo.x=primo.x+0.01*i*size*2/3;	
				primo.y=-0.95f*primo.x;
				currWidth=primo.x;
			}
			else if(size-currDecrease > 0.05f){
				primo.x= size - currDecrease;
				primo.y=-0.75f*primo.x;
				currDecrease = currDecrease*1.2;
				if(primo.y>=newReference.y){
				primo.y=newReference.y;
				tempPrimo=primo;
				}
			}
			else{	
					break;		
			}			
		}
		else{
			primo.x = -primo.x;
			if(fadeDecrease==1){
			newReference = rotateX(newReference + vec3(0.0f,0.0f,0.1f),1.0f);
			}
		}

		vertexBuffer[i].color = vec3(1.0f, 1.0f, 0.0f);
		vertexBuffer[i].position = primo;
		vertexBuffer[i].normal = vec3(0.0f, 1.0f, 0.0f);
		vertexBuffer[i+1].color = vec3(0.0f, 1.0f, 0.0f);
		vertexBuffer[i+1].position = secundo;
		vertexBuffer[i+1].normal = vec3(0.0f, 1.0f, 0.0f);
		vertexBuffer[i+2].color = vec3(0.0f+ linearRand(0.0f,0.1f), 0.4f+ linearRand(0.0f,0.3f), 0.0f);
		vertexBuffer[i+2].position = tertio;
		vertexBuffer[i+2].normal = vec3(0.0f+ linearRand(0.0f,0.1f), 0.4f+ linearRand(0.0f,0.3f), 0.0f);
		flip = !flip;
	}

	numOfVertices = sizeof(vertexBuffer) / sizeof(Vertex);
	// Create a vertex array
	glGenVertexArrays(1, &mVertexArrayID);
	// Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
	glGenBuffers(1, &mVertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
}

PalmLeaf::~PalmLeaf()
{
	// Free the GPU from the Vertex Buffer
	glDeleteBuffers(1, &mVertexBufferID);
	glDeleteVertexArrays(1, &mVertexArrayID);
}

void PalmLeaf::Update(float dt)
{
	// If you are curious, un-comment this line to have spinning cubes!
	// That will only work if your world transform is correct...
	// mRotationAngleInDegrees += 90 * dt; // spins by 90 degrees per second

	Model::Update(dt);
}

void PalmLeaf::Draw()
{
	// Draw the Vertex Buffer
	// Note this draws a unit Cube
	// The Model View Projection transforms are computed in the Vertex Shader
	glBindVertexArray(mVertexArrayID);

	GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform"); 
	glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
	
	// 1st attribute buffer : vertex Positions
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
							3,				// size
							GL_FLOAT,		// type
							GL_FALSE,		// normalized?
							sizeof(Vertex), // stride
							(void*)0        // array buffer offset
						);

	// 2nd attribute buffer : vertex normal
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	1,
							3,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
						);


	// 3rd attribute buffer : vertex color
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	2,
							3,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
						);

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, numOfVertices); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)

	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
}

bool PalmLeaf::ParseLine(const std::vector<ci_string> &token)
{
	if (token.empty())
	{
		return true;
	}
	else
	{
		return Model::ParseLine(token);
	}
}
