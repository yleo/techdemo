//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 15/7/15.
//         with help from Jordan Rutty
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "ParticleEmitter.h"
#include "Model.h"
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

using namespace glm;
using namespace std;

ParticleEmitter::ParticleEmitter(glm::vec3 position, const Model* parent)
: mpParent(parent), mPosition(position)
{
}


// This is a point emitter, nothing is random
// As a small extra task, you could create derived classes
// for more interesting emitters such as line emitters or circular emitters
// In these cases, you would sample a random point on these shapes
glm::vec3 ParticleEmitter::GetRandomPosition()
{
    // @TODO 7 - Position from Parented Emitter
    //
    // Return the position where the particle is emitted.
    // If the emitter is parented, the position is relative to its parent
    
    glm::vec3 position;
    
    mat4 emitterMatrix(1.0);
    
    if (mpParent){
        emitterMatrix = mpParent->GetWorldMatrix() * emitterMatrix;
        position += vec3(emitterMatrix[3][0], emitterMatrix[3][1], emitterMatrix[3][2]);
    }
    else{
            position = mPosition;
            mat4 t = translate(mat4(1.0f), mpParent->GetPosition());
            mat4 r = rotate(mat4(1.0f), mpParent->GetRotationAngle(), mpParent->GetRotationAxis());
            mat4 s = scale(mat4(1.0f), mpParent->GetScaling());
            emitterMatrix = t * r * s;
    }
    
    return position;
}




