//
// COMP 371 Assignment Framework
//
// Created by Nicolas Bergeron on 8/7/14.
// Updated by Gary Chang on 14/1/15
//
// Copyright (c) 2014-2015 Concordia University. All rights reserved.
//

#include "HillModel.h"
#include "Renderer.h"
#include "World.h"
#include "Camera.h"

// Include GLEW - OpenGL Extension Wrangler
#include <GL/glew.h>

using namespace glm;

HillModel::HillModel(vec3 size) : Model()
{
	// Create Vertex Buffer for all the verices of the Cube
	vec3 halfSize = size * 0.5f;
    
   
	
	Vertex vertexBuffer[] = {  // position,                normal,                  color
                //[0][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f) },
        { vec3(halfSize.x, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f) },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2( 1.0f, 0.0f)},
        
        { vec3(halfSize.x, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        //[0][1]
        { vec3(halfSize.x+1, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[0][2]
        { vec3(halfSize.x+2, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[0][3]
        { vec3(halfSize.x+3, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[0][4]
        { vec3(halfSize.x+4, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[0][5]
        { vec3(halfSize.x+5, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[0][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        //[1][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y+0.3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+0.3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[1][1]
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+0.3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[1][2]
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[1][3]
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[1][4]
        { vec3(halfSize.x+4, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[1][5]
        { vec3(halfSize.x+5, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[1][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+1), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        //[2][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y+0.3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[2][1]
        { vec3(halfSize.x+1, halfSize.y+0.3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y+0.3, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[2][2]
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[2][3]
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[2][4]
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[2][5]
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[2][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+2), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        //[3][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[3][1]
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[3][2]
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[3][3]
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[3][4]
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[3][5]
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[3][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+3), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        //[4][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[4][1]
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[4][2]
        { vec3(halfSize.x+2, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        //[4][3]
        { vec3(halfSize.x+3, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[4][4]
        { vec3(halfSize.x+4, halfSize.y+3, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[4][5]
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[4][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+4), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        //[5][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[5][1]
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[5][2]
        { vec3(halfSize.x+2, halfSize.y+2, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        //[5][3]
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[5][4]
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[5][5]
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+1.5, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[5][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+5), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        //[6][0]
        { vec3(halfSize.x, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        //[6][1]
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+1, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        //[6][2]
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+3, halfSize.y+1.5, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+2, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+1.5, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[6][3]
        { vec3(halfSize.x+3, halfSize.y+1, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+3, halfSize.y+1.5, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+3, halfSize.y+1.5, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+1.5, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[6][4]
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+4, halfSize.y+1.5, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+4, halfSize.y+2, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[6][5]
        { vec3(halfSize.x+5, halfSize.y+1.75, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+5, halfSize.y+1, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        
        { vec3(halfSize.x+5, halfSize.y+1, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(1.0f, 0.0f)  },
        //[6][6]
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 0.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  },
        
        { vec3(halfSize.x+6, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f), vec2(0.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+6), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 1.0f)  },
        { vec3(halfSize.x+7, halfSize.y, halfSize.z+7), vec3(-1.0f, 0.0f, 0.0f), vec4(1.0f, 1.0f, 1.0f, 0.0f), vec2(1.0f, 0.0f)  }
        
    };

	// Create a vertex array
	glGenVertexArrays(1, &mVertexArrayID);

	// Upload Vertex Buffer to the GPU, keep a reference to it (mVertexBufferID)
	glGenBuffers(1, &mVertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBuffer), vertexBuffer, GL_STATIC_DRAW);
}

HillModel::~HillModel()
{
	// Free the GPU from the Vertex Buffer
	glDeleteBuffers(1, &mVertexBufferID);
	glDeleteVertexArrays(1, &mVertexArrayID);
}

void HillModel::Update(float dt)
{

	Model::Update(dt);
}

void HillModel::Draw()
{
    ShaderType oldShader = (ShaderType)Renderer::GetCurrentShader();
    Renderer::SetShader(SHADER_TEXTURED);
    glUseProgram(Renderer::GetShaderProgramID());
    
    
    // This looks for the MVP Uniform variable in the Vertex Program
    GLuint VPMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "ViewProjectionTransform");
    
    // Send the view projection constants to the shader
    const Camera* currentCamera = World::GetInstance()->GetCurrentCamera();
    mat4 VP = currentCamera->GetViewProjectionMatrix();
    glUniformMatrix4fv(VPMatrixLocation, 1, GL_FALSE, &VP[0][0]);
    
    
    GLuint textureLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "myTextureSampler");
    glActiveTexture(GL_TEXTURE0);
    Renderer::CheckForErrors();
    glBindTexture(GL_TEXTURE_2D, mTexID);

    //glEnable(GL_TEXTURE_2D);
    glUniform1i(textureLocation, 0);
    
   
  
    
    glBindVertexArray(mVertexArrayID);

	GLuint WorldMatrixLocation = glGetUniformLocation(Renderer::GetShaderProgramID(), "WorldTransform"); 
	glUniformMatrix4fv(WorldMatrixLocation, 1, GL_FALSE, &GetWorldMatrix()[0][0]);
	
    
    
    // 1st attribute buffer : vertex Positions
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	0,				// attribute. No particular reason for 0, but must match the layout in the shader.
							4,				// size
							GL_FLOAT,		// type
							GL_FALSE,		// normalized?
							sizeof(Vertex), // stride
							(void*)0        // array buffer offset
						);

	// 2nd attribute buffer : vertex normal
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	1,
							4,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*)sizeof(vec3)    // Normal is Offseted by vec3 (see class Vertex)
						);


	// 3rd attribute buffer : vertex color
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glVertexAttribPointer(	2,
							4,
							GL_FLOAT,
							GL_FALSE,
							sizeof(Vertex),
							(void*) (2* sizeof(vec3)) // Color is Offseted by 2 vec3 (see class Vertex)
						);
    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
    glVertexAttribPointer(  3,
                            2,
                            GL_FLOAT,
                            GL_FALSE,
                            sizeof(Vertex),
                            (void*) (2*sizeof(vec3) + sizeof(vec4))
                        );

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, 295); // 36 vertices: 3 * 2 * 6 (3 per triangle, 2 triangles per face, 6 faces)
    
    glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
    Renderer::SetShader(oldShader);
    glUseProgram(Renderer::GetShaderProgramID());
}

bool HillModel::ParseLine(const std::vector<ci_string> &token)
{
	if (token.empty())
	{
		return true;
	}
	else
	{
		return Model::ParseLine(token);
	}
}
